import sys
import io

import prvi_zadatak


def breadthFirstSearch(pocetno_stanje, konacno_stanje, prijelazi):
    visited = []
    open = []
    towns = {}  #svi posjeceni gradovi i njegovi prethodnici

    open.append(pocetno_stanje)
    towns[pocetno_stanje] = ""
    while open:
        n = open.pop(0)
        if n not in visited:
            visited.append(n)

        for neighbour in prijelazi[n]:
            state = neighbour.strip().split(",")[0]
            if state not in visited and state not in open:
                open.append(state)
            if state not in towns.keys():
                towns[state] = n
        if n in konacno_stanje:
            break

    start = n   #konacno stanje
    end = pocetno_stanje
    open[:] = []
    open.append(start)
    while True:
        if start == end:
            break

        if start not in towns.keys():
            return False
        previous_town = towns[start]
        open.insert(0, previous_town)   #u open spremam path tako da idem od cilja prema startu
        start = previous_town

    return len(visited), len(open), open


def main():
    file1 = io.open(sys.argv[1], mode="r", encoding="utf-8")
    pocetno_stanje, konacno_stanje, prijelazi = prvi_zadatak.prostorStanja(file1)
    visited, length, path = breadthFirstSearch(pocetno_stanje, konacno_stanje.split(" "), prijelazi)
    print("\nRunning bfs: \nStates visited = "+str(visited)+"\nFound path of length "+str(length) +":")
    for town in path:
        print(town+" =>")

if __name__ == "__main__":
    main()
