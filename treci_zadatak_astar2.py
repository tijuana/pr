from operator import itemgetter

# ------------------------------------------------------------------------------------------------------------------------------------------#
# ova implementacija sluzi samo da bi algoritam A* isao do kraja kako bih mogla dobit popis gradova i njegovih prethodnih
# gradova sto je sadrzano u dictionary-u "towns_prices". A to mi je potrebno pri izracunavanju konzistentnosti
# ------------------------------------------------------------------------------------------------------------------------------------------#


def aStarSearch(pocetno_stanje, konacno_stanje, prijelazi, heuristic):
    visited = []
    open = []
    towns_prices = {}

    open.append((pocetno_stanje, 0))
    towns_prices[pocetno_stanje] = ["", 0]
    while open:
        stanje = open.pop(0)
        n = stanje[0]
        price = towns_prices[n][1]

        zastavica = 0
        for i in range(len(visited)):
            if visited[i][0] == n:
                zastavica = 1  # grad je posjecen
        if zastavica == 0:  # grad nije posjecen
            visited.append((n, price + int(heuristic[n])))

        if n in konacno_stanje:
            konacan=n

        for neighbour in prijelazi[n]:
            state = neighbour.strip().split(",")
            zastavica = 0
            for i in range(len(open)):
                if open[i][0] == state[0]:
                    if price + int(heuristic[state[0]]) + int(state[1]) < open[i][1]:
                        del open[i]
                        open.append(
                            (state[0], int(state[1]) + price + int(heuristic[state[0]]))
                        )
                        open.sort(key=itemgetter(0))
                        open.sort(key=itemgetter(1))
                    zastavica = 1  # oznaka da je grad u openu-ne dodati naknadno
            for i in range(len(visited)):
                if visited[i][0] == state[0]:
                    if price + int(heuristic[state[0]]) + int(state[1]) < int(
                        visited[i][1]
                    ):
                        if zastavica == 1:  # brisi ga iz opena samo ako je u openu
                            del open[i]
                        open.append(
                            (state[0], int(state[1]) + price + int(heuristic[state[0]]))
                        )
                        open.sort(key=itemgetter(0))
                        open.sort(key=itemgetter(1))
                    zastavica = 1  # oznaka da je grad u visitedu-ne dodati naknadno
            if zastavica == 0:  # grad nije nigdje-dodaj u open
                open.append(
                    (state[0], int(state[1]) + price + int(heuristic[state[0]]))
                )
                open.sort(key=itemgetter(0))
                open.sort(key=itemgetter(1))

            if state[0] not in towns_prices.keys() or towns_prices[state[0]][
                1
            ] > price + int(state[1]):
                towns_prices[state[0]] = [n, price + int(state[1])]

    start = konacan
    end = pocetno_stanje
    open[:] = []
    open.append(start)
    while True:
        if start == end:
            break
        if start not in towns_prices.keys():
            return False
        previous_town = towns_prices[start][0]
        open.insert(0, previous_town)
        start = previous_town

    return len(visited), open, towns_prices[konacan][1], towns_prices
