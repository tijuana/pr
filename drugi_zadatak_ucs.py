import sys
import io
from operator import itemgetter

import prvi_zadatak

def uniformCostSearch(pocetno_stanje,konacno_stanje,prijelazi):
    visited = []
    open = []
    towns_prices = {}   #lista gradova i njegovih prethodnika te ukupna cijena do tog grada

    open.append((pocetno_stanje, 0))    #uz svaki grad se nalazi i konacna cijena do tog grada
    towns_prices[pocetno_stanje] = ["", 0]
    while open:
        stanje = open.pop(0)
        n = stanje[0]
        if n not in visited:
            visited.append(n)
        price = towns_prices[n][1]

        if n in konacno_stanje:
            break

        for neighbour in prijelazi[n]:
            state = neighbour.strip().split(",")
            zastavica=0
            for i in range(len(open)):
                if open[i][0]==state[0] and int(state[1])+price<open[i][1]: #ako se grad vec nalazi u openu ali novi nacin je jeftiniji nacin
                    del open[i]
                    open.append((state[0],int(state[1])+price))
                    open.sort(key=itemgetter(0))
                    open.sort(key=itemgetter(1))

                    zastavica = 1
                if open[i][0]==state[0]:
                    zastavica = 1
            if zastavica==0 and state[0] not in visited: #nije u open listi->ubaci ga
                open.append((state[0],int(state[1])+price))
                open.sort(key=itemgetter(0))
                open.sort(key=itemgetter(1))

            if state[0] not in towns_prices.keys() or towns_prices[state[0]][1] > price + int(state[1]):
                towns_prices[state[0]]=[n, price+int(state[1])]

    start = n
    end = pocetno_stanje
    open[:]=[]
    open.append(start)
    while True:
        if start == end:
            break
        if start not in towns_prices.keys():
            return False
        previous_town = towns_prices[start][0]
        open.insert(0,previous_town)    #u open spremam path tako da idem od cilja prema startu
        start = previous_town

    return len(visited),towns_prices[n][1],open

def main():
    file1 = io.open(sys.argv[1], mode="r", encoding="utf-8")
    pocetno_stanje,konacno_stanje,prijelazi=prvi_zadatak.prostorStanja(file1)
    visited,price,path = uniformCostSearch(pocetno_stanje,konacno_stanje.split(" "),prijelazi)
    print("\nRunning ucs: \nStates visited = " + str(visited) + "\nFound path of length " + str(len(path)) + " with total cost : "+str(price))
    for town in path:
        print(town + " =>")

if __name__ == "__main__":
     main()
