import sys
import io

global pocetno_stanje, konacno_stanje, prijelazi
global heuristicValues


def prostorStanja(tekst):
    global pocetno_stanje, konacno_stanje, prijelazi
    linija = tekst.readline().strip()

    if "#" in linija:
        linija = tekst.readline().strip()

    pocetno_stanje = linija
    konacno_stanje = tekst.readline().strip()

    prijelazi = {}

    for s in tekst:
        linija = s.strip()
        polje = linija.split(" ")
        vrijednost = []
        for i in range(1, len(polje)):
            vrijednost.append(polje[i])
        prijelazi.update({polje[0][:-1]: vrijednost})
    return pocetno_stanje, konacno_stanje, prijelazi


def opisnikHeuristike(ulaz):
    global heuristicValues
    heuristicValues = {}
    for s in ulaz:
        linija = s.strip()
        polje = linija.split(" ")
        heuristicValues.update({polje[0][:-1]: polje[1]})
    return heuristicValues


def main():
    global pocetno_stanje, konacno_stanje, prijelazi
    global heuristicValues

    # -------------------------------------metode za ucitavanje podataka iz navedenog formata------------------------------------#

    file1 = io.open(sys.argv[1], mode="r", encoding="utf-8")
    file2 = io.open(sys.argv[2], mode="r", encoding="utf-8")
    pocetno_stanje,konacno_stanje,prijelazi = prostorStanja(file1)
    heuristicValues = opisnikHeuristike(file2)
	
    total = 0
    for k in prijelazi.values():
	    total = total + len(k)
	
    print("Start state: "+str(pocetno_stanje)+
	"\nEnd state(s): "+str(konacno_stanje.split(" "))+
	"\nState space size: "+str(len(prijelazi))+
	"\nTotal transitions: "+str(total))


if __name__ == "__main__":
    main()
