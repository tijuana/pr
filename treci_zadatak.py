import sys
import io

import prvi_zadatak
import treci_zadatak_astar2


def checkOptimistic(heuristic_values, pocetno_stanje, konacno_stanje, prijelazi):
    flag = 0
    for n in heuristic_values.keys():
        a, b, cost, _ = treci_zadatak_astar2.aStarSearch(
            n, konacno_stanje, prijelazi, heuristic_values
        )
        if int(heuristic_values[n]) > cost:
            flag = 1  # nije optimisticna
            print(
                "[ERR] h("
                + str(n)
                + ") > h*: "
                + str(heuristic_values[n])
                + " > "
                + str(cost)
            )
    if flag == 0:
        print("Heuristic is optimistic")
    if flag == 1:
        print("Heuristic is not optimistic")


def checkConsistency(
    heuristic_values, pocetno_stanje, konacno_stanje, prijelazi, towns
):
    flag = 0
    #r_keys = reversed(list(heuristic_values.keys()))    #ovo radim da bi ispis bio dobar,inace je tocno sve
    for n in heuristic_values.keys():
        previous = towns[n][0]
        if previous == "":
            previous = pocetno_stanje
        a, b, cost, _ = treci_zadatak_astar2.aStarSearch(
            previous, n, prijelazi, heuristic_values
        )
        if int(heuristic_values[previous]) > int(heuristic_values[n]) + cost:
            flag = 1  # nije konzistentno
            print(
                "[ERR] h("
                + str(previous)
                + ") > h("
                + str(n)
                + ") + c: "
                + str(heuristic_values[previous])
                + " > "
                + str(heuristic_values[n])
                + " + "
                + str(cost)
            )
    if flag == 0:
        print("Heuristic is consistent")
    if flag == 1:
        print("Heuristic is not consistent")


def main():
    file1 = io.open(sys.argv[1], mode="r", encoding="utf-8")
    file2 = io.open(sys.argv[2], mode="r", encoding="utf-8")

    pocetno_stanje, konacno_stanje, prijelazi = prvi_zadatak.prostorStanja(file1)
    heuristic_values = prvi_zadatak.opisnikHeuristike(file2)

    print("\nChecking if heuristic is optimistic.")
    checkOptimistic(heuristic_values, pocetno_stanje, konacno_stanje.split(" "), prijelazi)

    print("\nChecking if heuristic is consistent.")
    a, b, c, towns = treci_zadatak_astar2.aStarSearch(
        pocetno_stanje, konacno_stanje, prijelazi, heuristic_values
    )
    checkConsistency(heuristic_values, pocetno_stanje, konacno_stanje, prijelazi, towns)


if __name__ == "__main__":
    main()
